import Home from "./Views/Home.svelte"
import InvestorsList from "./Views/Investors/index.svelte"
import ProjectIndex from "./Views/Projects/index.svelte"
import ProjectCreate from "./Views/Projects/create.svelte"
import ProjectShow from "./Views/Projects/show.svelte"
import ProjectUpdate from "./Views/Projects/update.svelte"
import InvestorCreate from "./Views/investors/create.svelte"
import InvestorsIndex from "./Views/Investors/index.svelte"
import InvestmentCreate from "./Views/Investments/InvestmentCreate.svelte"
import InvestmentProjectList from "./Views/Investments/InvestmentProjectList.svelte"
import InvestmentInvestorList from "./Views/Investments/InvestmentInvestorList.svelte"

export const routes = {
    "/": ProjectIndex,

    // projects routes
    "/projects": ProjectIndex,
    "/projects/create": ProjectCreate,
    "/projects/:id": ProjectShow,
    "/projects/:id/update": ProjectUpdate,
    "/projects/:id/invest": InvestmentCreate,
    "/projects/:id/investments": InvestmentProjectList,

    // investors routes
    "/investors": InvestorsIndex,
    "/investors/create": InvestorCreate,
    "/my-investments": InvestmentInvestorList,

    // investments routes
    "/investments/create": InvestorsList,
}