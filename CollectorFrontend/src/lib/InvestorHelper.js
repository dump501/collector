/**
 *
 */

import { ethers } from "ethers";
import ContractAddresses from "../lib/ContractsAddresses.json";
import Collector from "../Contracts/Collector.json";

/**
 * create an investor on the blockchain
 * @param {string} name
 * @param {string} address
 */
export const createInvestor = async (name, address) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.addInvestor(name, address);
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured maket sure you are connected with the manager account")
  }
};

export const getInvestors = async () => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.getIvestors();
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you have a good connection")
  }
};

export const getInvestor = async (investorId) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.getInvestor(investorId);
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure the investor exists");
  }
};

