/**
 *
 */

import { ethers } from "ethers";
import ContractAddresses from "../lib/ContractsAddresses.json";
import Collector from "../Contracts/Collector.json";

/**
 * create a project on the blockchain
 * @param {string} name
 * @param {string} whitePaper
 * @param {string} owner
 */
export const createProject = async (name, whitePaper, owner) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.addProject(name, whitePaper, owner);
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you are connected with the manager account")
  }
};

export const getProjects = async () => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.getProjects();
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you have a good internet connection")
  }
};

export const getProject = async (id) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.getProject(id);
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you have a good internet connection")
  }
};

export const updateProjectState = async (id, state) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.updateProjectState(id, state);
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you are connected with the manager account")
  }
};

export const withdrawFunds = async (projectId) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    await collectorContract.withdrawFunds(projectId);
    alert("fund transfered successfully");
  } catch (error) {
    console.error(error)
    alert("Error white processing the transaction")
    return 
  }
};

