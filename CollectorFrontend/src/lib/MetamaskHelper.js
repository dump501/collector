  //check function to see if the MetaMask extension is installed
  export const isMetaMaskInstalled = () => {
    //Have to check the ethereum binding on the window object to see if it's installed
    // @ts-ignore
    const { ethereum } = window;
    return Boolean(ethereum && ethereum.isMetaMask);
  };