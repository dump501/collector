/**
 *
 */

import { ethers } from "ethers";
import ContractAddresses from "../lib/ContractsAddresses.json";
import Collector from "../Contracts/Collector.json";

/**
 * create a project on the blockchain
 * @param {Number} projectId
 * @param {Number} price
 */
export const invest = async (projectId, price) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let transaction = await collectorContract.invest(projectId, {value: ethers.utils.parseUnits(price.toString(), 'ether')});
    return transaction;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure the manager registrated you as an investor")
  }
};

export const getProjectInvestment = async (projectId) => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let tx = await collectorContract.getProjectInvestments(projectId);
    return tx;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you have a good internet connection")
  }
};

export const getInvestorInvestments = async () => {
  // @ts-ignore
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  const signer = provider.getSigner();
  const collectorContract = new ethers.Contract(
    ContractAddresses.collector,
    Collector.abi,
    signer
  );

  try {
    let tx = await collectorContract.getInvestorInvestments();
    return tx;
  } catch (error) {
    console.error(error)
    alert("An error occured make sure you the manager registrated you as investor")
  }
};
