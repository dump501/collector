// SPDX-License-Identifier: GPL-3.0
pragma solidity =0.8.17;

// imports
import "@openzeppelin/contracts/utils/Counters.sol";

/// @title Collector: manages the investors, the projects and the investments
/// @author Tsafack Fritz <tsafack07albin@gmail.com>
/// @notice manages the investors, the projects and the investments
/// @dev manages the investors, the projects and the investments
contract Collector{
    // counters 
    using Counters for Counters.Counter;
    Counters.Counter public investmentCount;
    Counters.Counter public investorCount;
    Counters.Counter public projectCount;

    // different states of the project
    enum State { Created, Investing, Ended }

    // structs
    struct Investment{
        uint projectId;
        uint price;
        uint investorId;
    }
    struct Investor{
        string name;
        address investorAddress;
    }
    struct Project{
        string title;
        string whitePaper;
        State state;
        address owner;
        uint investedValue;
        uint currentInvestemntValue;
    }

    // public addresses
    address public manager;

    mapping(uint => Investment) private investments;
    mapping(uint => Investor) private investors;
    mapping(uint => Project) private projects;
    mapping(uint => uint) private investmentToProject;

    // events
    event InvestorAdded(string name, address investorAddress);
    event ProjectAdded(string title, string whitePaper, address owner);
    event InvestmentAdded(uint projectId, uint price, uint investorId);

    // modifier
    modifier onlyManager(){
        require(msg.sender == manager, "You are not authorize to perform this action");
        _;
    }

    modifier projectIs(uint projectId, State state){
        require(projects[projectId].state == state, "we are not yet inveting on the project please wait");
        _;
    }

    constructor(){
        manager = msg.sender;
    }

    // Todo: add function to change manager
    function updateManager(address _manager) public onlyManager returns(address){
        manager = _manager;
        return _manager;
    }

    /// @notice get the project detail of the given project Id
    /// @param projectId the project id we want to get details
    /// @return Project the project details
    function getProject(uint projectId) public view returns(Project memory){
        return projects[projectId];
    }

    /// @notice get the investment detail of the given investment Id
    /// @param investmentId the investment id we want to get details
    /// @return Ivestment the investment details
    function getInvestment(uint investmentId) public view returns(Investment memory){
        return investments[investmentId];
    }


    /// @notice get the investor detail of the given investor Id
    /// @param investorId the investor id we want to get details
    /// @return Ivestor the investor details
    function getInvestor(uint investorId) public view returns(Investor memory){
        return investors[investorId];
    }


    /// @notice get the projects
    /// @dev loop throught the projects mapping, insert in an array and return the array
    /// @return Project[] the array of projects
    function getProjects() public view returns(Project[] memory){
        uint currentIndex =0;
        uint count = projectCount.current();
        Project[] memory result = new Project[](count);

        for(uint i=1; i<=count; i++){
            result[currentIndex] = projects[i];
            currentIndex++;
        }

        return result;
    }

    /// @notice get the investments
    /// @dev loop throught the investments mapping, insert in an array and return the array
    /// @return Investment[] the array of investments
    function getInvestments() public view returns(Investment[] memory){
        uint currentIndex =0;
        uint count = investmentCount.current();
        Investment[] memory result = new Investment[](count);

        for(uint i=1; i<=count; i++){
            result[currentIndex] = investments[i];
            currentIndex++;
        }

        return result;
    }

    /// @notice get the investors
    /// @dev loop throught the investors mapping, insert in an array and return the array
    /// @return Investor[] the array of Investors
    function getIvestors() public view returns(Investor[] memory){
        uint currentIndex =0;
        uint count = investorCount.current();
        Investor[] memory result = new Investor[](count);

        for(uint i=1; i<=count; i++){
            result[currentIndex] = investors[i];
            currentIndex++;
        }

        return result;
    }

    /// @notice add an investor. it can only be done by the manager
    /// @dev add an investor to the investors mapping
    /// @param _name the investor's name
    /// @param _investor the investor's address
    /// @return Investor the newly added investor
    function addInvestor(string memory _name, address _investor)
        public onlyManager 
        returns(Investor memory){
        // check investor name already exists
        investorCount.increment();
        investors[investorCount.current()] = Investor(_name, payable(_investor));
        emit InvestorAdded(_name, _investor);
        return investors[investorCount.current()];
    }

    /// @notice permit an existing investor to invest on a project with investing state
    /// @dev verify if investor exists and transfer sended value to the contract
    /// @param projectId the project the investor wants to invest on
    /// @return Investment the newly added investment
    function invest(uint projectId)
        public payable projectIs(projectId, State.Investing) 
        returns(Investment memory){
        require(msg.value > 0, "you should send some ether to invest");
        // check if project is on investing
        uint investorId = 0;
        for(uint i=1; i<= investorCount.current(); i++){
            if(msg.sender == investors[i].investorAddress){
                investorId = i;
            }
        }

        require(investorId >0, "The investor doesn't exists");

        investmentCount.increment();
        investments[investmentCount.current()] = Investment(projectId, msg.value, investorId);
        projects[projectId].currentInvestemntValue += msg.value;
        emit InvestmentAdded(projectId, msg.value, investorId);
        return investments[investmentCount.current()];
    }

    /// @notice get the investments for a given product id
    /// @dev loop throught the investments mapping and return an 
    ///      array containing investment for the given project id
    /// @param projectId the project id we want get investment
    /// @return Investment[] the array of investment
    function getProjectInvestments(uint projectId) public view returns(Investment[] memory){
        uint count = 0;
        for(uint i=1; i<=investmentCount.current(); i++){
            if(investments[i].projectId == projectId){
                count++;
            }
        }
        uint currentIndex =0;
        Investment[] memory result = new Investment[](count);
        for(uint i=1; i<=investmentCount.current(); i++){
            if(investments[i].projectId == projectId){
                result[currentIndex] = investments[i];
                currentIndex++;
            }
        }

        return result;
    }

    /// @notice get the investments of an investor
    /// @dev loop throught the investments mapping and return an 
    ///      array containing investments of the sender
    /// @return Investment[] the array of investment
    function getInvestorInvestments() public view returns(Investment[] memory){
        uint investorId = 0;
        for(uint i=1; i<=investorCount.current(); i++){
            if(investors[i].investorAddress == msg.sender){
                investorId = i;
            }
        }

        require(investorId > 0, "The investor not exist");

        uint count = 0;
        for(uint i=1; i<=investmentCount.current(); i++){
            if(investments[i].investorId == investorId){
                count++;
            }
        }

        uint currentIndex =0;
        Investment[] memory result = new Investment[](count);

        for(uint i=1; i<=investmentCount.current(); i++){
            if(investments[i].investorId == investorId){
                result[currentIndex] = investments[i];
                currentIndex++;
            }
        }

        return result;
    }

    /// @notice changes a project state. It can only be done by the manager
    /// @dev update the given project state
    /// @param projectId the project's id
    /// @param state the project's state
    /// @return Project the updated project
    function updateProjectState(uint projectId, uint state) public onlyManager returns(Project memory){
        // state = 1 => investing
        // state = 0 => ended
        if(state == 1){
            projects[projectId].state =  State.Investing;
        } 
        if(state == 0){
            projects[projectId].state =  State.Ended;
        }
        return projects[projectId];
    }

    /// @notice add a new project. it can only be done by the manager
    /// @dev add a project to the projects mapping
    /// @param _title the project's title
    /// @param _whitePaper the project's white paper url.
    /// @param _owner the project's owner address
    /// @return Project the newly added project
    function addProject(string memory _title, string memory _whitePaper, address _owner) public onlyManager returns(Project memory){
        projectCount.increment();
        projects[projectCount.current()] = Project(_title, _whitePaper, State.Created, payable(_owner), 0, 0);
        emit ProjectAdded(_title, _whitePaper, _owner);
        return projects[projectCount.current()];
    }

    /// @notice transfert invested funds to the project's owner
    /// @dev transfert invested funds to the project's owner
    /// @param projectId the project id we want to withdraw funds
    function withdrawFunds(uint projectId) public payable {
        Project storage project = projects[projectId];
        require(project.owner == msg.sender, "You can only withdraw funds on you project");
        require(project.currentInvestemntValue >0, "There are not yet funds");
        (bool sent,)=project.owner.call{value: project.currentInvestemntValue}("");

        require(sent, "withdraw failed !");

        project.investedValue += project.currentInvestemntValue;
        project.currentInvestemntValue = 0;
    }
}