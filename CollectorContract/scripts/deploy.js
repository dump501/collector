// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const { ethers } = require("hardhat");
const hre = require("hardhat");
const fs = require("fs");

async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deploying contract with account:", deployer.address);
  console.log("Account balance:", (await deployer.getBalance()).toString());

  const Collector = await hre.ethers.getContractFactory("Collector");
  const collector = await Collector.deploy();
  await collector.deployed();
  console.log("Contract address:", collector.address);

  const data = {
    address: collector.address,
    abi: JSON.parse(collector.interface.format('json'))
  }

  // write the json file on the dis
  fs.writeFileSync('./build/Collector.json', JSON.stringify(data));
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
